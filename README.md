# jsoneditor-theme-semantic

[JSON Editor](https://github.com/json-editor/json-editor) Semantic UI theme

## Installation

```javascript
import { JSONEditor } from "@json-editor/json-editor/src/core";
import SemanticUITheme from "@gaia/jsoneditor-theme-semantic";

JSONEditor.defaults.themes.semantic_ui = SemanticUITheme;
JSONEditor.defaults.theme = "semantic_ui";
```
