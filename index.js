import { AbstractTheme } from "@json-editor/json-editor/src/theme";
import "./semanticui.css"

export default class SemanticUITheme extends AbstractTheme {

    constructor (jsoneditor) {
        super(jsoneditor, {
            show_error_message: false,
        })
    }

    /** Wrapper **/

    getContainer() {
        const el = document.createElement('div');
        el.className = 'ui form';
        return el;
    }

    getIndentedPanel() {
        const el = document.createElement('div');
        el.className = 'ui segment';
        return el;
    }

    /** Form **/

    getFormControl(label, input, description) {
        const group = document.createElement('div');

        if(label && input.type === 'checkbox') {
            const field = document.createElement('div');
            field.className += ' ui toggle checkbox';
            field.appendChild(input);
            field.appendChild(label);

            group.className += ' field';
            group.appendChild(field);
        }
        else {
            group.className += ' field';
            if(label) group.appendChild(label);
            group.appendChild(input);
        }

        if(label && description) {
            label.appendChild(description)
        }

        return group;
    }

    getFormInputField(type) {
        const el = super.getFormInputField(type);
        if(type !== 'checkbox') {
            el.className += ' ui input';
        }
        return el;
    }

    getFormInputLabel(text, req) {
        const h4 = document.createElement('span');
        h4.appendChild(document.createTextNode(text));

        const el = document.createElement('label');
        el.appendChild(h4);

        return el;
    }

    getTextareaInput() {
        const el = document.createElement('textarea');
        el.setAttribute('rows', 2);
        return el;
    }

    /** Buttons **/

    getHeaderButtonHolder() {
        const el = this.getButtonHolder();
        el.style.float = 'right';
        return el;
    }

    getButton(text, icon, title) {
        const el = super.getButton(text, icon, title);
        el.className += ' ui button basic mini';
        if(text) el.className += ' labeled icon';
        if(text && icon) icon.className += ' icon';
        return el;
    }

    /** Element **/

    getTable() {
        const el = document.createElement('table');
        el.className = 'ui table bordered';
        return el;
    }

    getDescription(text) {
        const el = document.createElement('small');
        el.appendChild(document.createTextNode(` (${text})`));
        return el;
    }

    /** Errors **/

    getErrorMessage(text) {
        const group = document.createElement('div');
        group.className = 'ui negative message';

        if(this.options.show_error_message === false) {
            group.style.display = 'none';
        }

        const el = document.createElement('p');
        el.appendChild(document.createTextNode(text));

        group.appendChild(el);

        return group;
    }

    addInputError(input, text) {
        if(input.error) return;

        const el = document.createElement('p');
        el.className = 'ui pointing red basic label';
        el.innerHTML = text;

        input.parentElement.appendChild(el);
        input.parentElement.classList.add('error');

        input.error = text;
        if(input.dataset) input.dataset.error = text;

        this.handleTabErrors(input, (tabMenu, label, errors) => {
            if(errors.length < 1) return;

            if(label === null) {
                label = document.createElement('span');
                label.className = 'ui small red horizontal circular label';

                tabMenu.appendChild(label);
            }

            label.innerHTML = errors.length;
        });
    }

    removeInputError(input) {
        if(!input.error) return;

        const elements = input.parentElement.getElementsByClassName('ui pointing red basic label');
        if(elements.length === 0) return;

        input.parentElement.removeChild(elements.item(0));
        input.parentElement.classList.remove('error');

        delete input.error;
        if(input.dataset) delete input.dataset.error;

        this.handleTabErrors(input, (tabMenu, label, errors) => {
            if(errors.length > 0) {
                label.innerHTML = errors.length;
            }
            else if(label !== null) {
                tabMenu.removeChild(label);
            }
        });
    }

    handleTabErrors(input, callback) {
        const tabContentElement = input.closest('[data-type="top-tab-content"]');
        const tabMenuElement = tabContentElement.parentNode.querySelector('[data-type="top-tab-menu"]');

        Array.from(tabContentElement.childNodes).forEach((tabContent, index) => {
            const tabMenu = tabMenuElement.children[index];
            const label = tabMenu.children.length > 1 ? tabMenu.querySelector('.ui.label') : null;
            const errors = tabContent.querySelectorAll('[data-error]');

            callback(tabMenu, label, errors);
        });
    }

    /** Tab **/

    getTabHolder() {
        const el = document.createElement('div');
        el.className = 'ui grid segment';
        el.innerHTML = `
            <div class="three wide column">
                <div class="ui vertical fluid tabular menu"></div>
            </div>
            <div class="thirteen wide stretched column"></div>
        `;
        return el;
    }

    getTopTabContentHolder (tabHolder) {
        tabHolder.children[0].dataset.type = 'top-tab-menu';
        tabHolder.children[1].dataset.type = 'top-tab-content';

        return super.getTopTabContentHolder(tabHolder);
    }

    addTab(holder, tab) {
        holder.children[0].children[0].appendChild(tab);
    }

    getTab(span, tabId) {
        const el = document.createElement('a');
        el.className = 'item';
        el.appendChild(span);
        return el;
    }
}
